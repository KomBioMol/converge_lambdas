# converge_lambdas

A Python script that uses Gromacs to optimize the spacing of lambdas in replica-exchange thermodynamic integration (TI) alchemical free energy simulations. The spacing is adjusted so that exchange probabilities between neighboring windows are equalized.

## Getting Started

The script is ready to use on any machine employed with a working Python distribution. Run "python converge_lambdas.py -h" to see available options.

### Prerequisites

Any scientific Python distribution (such as Anaconda or WinPython) will typically contain all required libraries. Besides several built-in libraries (shutil, argparse, os, subprocess), only numpy is required for full functionality; if not available, it can be installed using pip:

```
pip install numpy
```

### Options

#### Standard usage

  The standard run consists of a cycle of iterative energy
  minimization/molecular dynamics runs that are performed automatically
  based on lambda dynamics-ready .gro (structure) and .top (topology)
  files. Accordingly, only three parameters are required, namely the
  initial .gro file (`-f`; in any lambda state, will be multiplied and
  optimized anyway), a compatible topology (`-p`; please check grompp
  before running) and the desired number of intermediate states (`-n`;
  preferably higher if the perturbation is large, otherwise exchanges
  will be rare).

  If the other state (lambda=1) was pre-equilibrated in a separate run
  (as would be considered good practice), the structure corresponding
  to this state can be passed using `-g`: this way the first half of
  structures will be initialized from the structure corresponding to
  lambda=0 (option `-f`), and the other - corresponding to lambda=1.

  The script runs Gromacs in parallel (by default using
  `mpiexec gmx_mpi mdrun`), so it's best to run it as a batch job on a
  cluster. A sample job submission script using the SLURM system would
  look as follows:

```
#!/bin/bash -l
#SBATCH -J opt-lambdas
#SBATCH -N 12
#SBATCH -n 288
#SBATCH --ntasks-per-node=24
#SBATCH --time=01:00:00

cd $SLURM_SUBMIT_DIR

module load plgrid/apps/gromacs/2016-s
module load plgrid/tools/python/3.6.0

python3.6 converge_lambdas.py -f ../mini.gro -p ../topol.top -n 24

```

  The algorithm works in the following manner:

  1. An equal lambda-spacing is assumed as a guess.
  2. Files are prepared for replica-exchange TI (minimization and dynamics).
  3. Mean exchange probabilities are inferred from .log files and
  running-averaged in an exponentially decaying manner (new_values =
  0.5\*new_values + 0.5\*old_values) to dampen fluctuations.
  4. Points are moved in the lambda-space to bring closer ones that do
  not exchange, and separate ones that exchange too frequently.
  5. In subsequent runs, the learning rate is steadily decreased and
  run time increased to facilitate convergence.
  6. The process is repeated until the minimum exchange probaility is
  greater than (1-threshold)*maximum exchange probability.


#### User-defined guess for lambda-spacing

  If the user wishes to initialize the algorithm with a specific set of
  lambda points, these can be provided in a separate (single-line) file
  and passed to the script with the -i option (note that the number of
  lambdas has to match the number of jobs specified using -n). In this
  way, one can restart an optimization that failed to converge using
  intermediate values stored in the file iterations.dat.

#### Tighter thresholds

  The default threshold (0.25) will typically allow the run to terminate
  after 10-20 iterations. As mean probabilities in individual runs tend
  to fluctuate, a more stringent criterion seems unnecessary; if desired,
  one can monitor the convergence of lambdas during the course of optimization:

```bash
cat iterations.dat | grep lambda | cut -d ' ' -f 2- > lambdas.convergence
```

and plot them in matplotlib:

```python
import numpy as np
import matplotlib.pyplot as plt
d = np.loadtxt('lambdas.convergence')
plt.plot(d)
plt.show()
```

### Other MPI launchers

  By default, Gromacs is run using `mpiexec`. If this is undesired, MPI launcher can be changed using the -m option.
