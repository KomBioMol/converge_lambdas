# -*- coding: utf-8 -*-
"""
The script runs Gromacs multiple times to determine the optimal lambda-spacing
for alchemical free energy simulations. It starts by assuming equal spacing
and iteratively stretches or compresses the intervals based on exchange
probabilities until desired convergence is achieved.

Created on Tue Dec  5 00:30:12 2017

@author: miloszw
"""
import numpy as np
from subprocess import call
from multiprocessing import Pool
import os
from shutil import copy2, rmtree
import argparse


def converge_lambdas(threshold, topfile, grofile, grofile2, mpiexec, njobs, initguess, xtc, xtc2, maxwarn, multidir,
                     hrex):
    mini_mdp = 'minimize.mdp'
    dyn_mdp = 'md.mdp'
    learning_rate = 2.0  # will multiply scaling factors, decreases over time
    nsteps = 10000  # starting value, increases over time
    lambdas = initialize_lambdas(njobs, initguess)
    # initialize probs
    current_probs = np.zeros(njobs-1)
    # set filenames and nsteps:
    prepare_inputs(njobs, dyn_mdp, nsteps, grofile=grofile, grofile2=grofile2, xtc=xtc, xtc2=xtc2)
    log_message("All systems set up, ready to go", mode='w')
    log_message("lambdas: {}".format(''.join(['{:8.4f}'.format(a) for a in lambdas])))
    # the 'or' sets up a lower limit on number of iterations
    iteration = 0
    topfile = topfile.split() if ' ' in topfile else [topfile for _ in range(njobs)]
    while np.max(current_probs) - np.min(current_probs) > threshold*np.max(current_probs) or learning_rate > 1.0:
        iteration += 1
        log_message("Running iteration {}...".format(iteration))
        run_minimization(topfile, mini_mdp, njobs, lambdas, mpiexec, maxwarn=maxwarn, multidir=multidir)
        clear_files()
        run_gromacs(topfile, dyn_mdp, njobs, lambdas, mpiexec, maxwarn=maxwarn, multidir=multidir, hrex=hrex)
        # sometimes calcs break and we get 'nan' as probs, so to avoid, we just restart:
        try:
            # sort-of running average to flatten out fluctuations in mean probs
            current_probs = 0.5*get_exchange_probs(njobs) + 0.5*current_probs
            lambdas = update_lambdas(lambdas, current_probs, learning_rate)
            clear_files()
            log_message("lambdas: {}".format(''.join(['{:8.4f}'.format(a) for a in lambdas])))
            log_message("probs: {}".format(''.join(['{:8.4f}'.format(a) for a in current_probs])))
            learning_rate *= 0.9
            nsteps *= 1.2
            prepare_inputs(njobs, dyn_mdp, nsteps)
        except ValueError:
            clear_files()
            prepare_inputs(njobs, dyn_mdp, nsteps, grofile=grofile, grofile2=grofile2)
            log_message('Sim crashed, trying again. If this happens frequently, check your system for stability.')
    return lambdas, current_probs


def prepare_minimized(topfile, grofile, grofile2, mpiexec, njobs, initguess, xtc, xtc2, maxwarn, mdir):
    mini_mdp = 'minimize.mdp'
    lambdas = initialize_lambdas(njobs, initguess)
    prepare_inputs(njobs, grofile=grofile, grofile2=grofile2, xtc=xtc, xtc2=xtc2)
    if mpiexec:
        run_minimization(topfile, mini_mdp, njobs, lambdas, mpiexec, maxwarn=maxwarn, multidir=mdir)
    else:
        run_minimization(topfile, mini_mdp, njobs, lambdas, mpiexec, serial=True, maxwarn=maxwarn, multidir=mdir)
    clear_files()
    for i in range(njobs):
        copy2('mymini{}.gro'.format(i), 'mygro{}.gro'.format(i))  # as a better guess for subsequent minimization
    for i in [x for x in os.listdir('.') if x.startswith('mymini')]:
        os.remove(i)


def parse_args():
    parser = argparse.ArgumentParser(description='Runs TI jobs iteratively to optimize lambda-spacing '
                                                 'for alchemical free energy simulations. '
                                                 'Assumes newer versions of gromacs compiled widh MPI, '
                                                 'i.e. uses gmx_mpi grompp and gmx_mpi mdrun.')
    parser.add_argument('-f', type=str, dest='gro', help='a .gro file with the initial structure (lambda=0)')
    parser.add_argument('-g', type=str, dest='gro2', default=None,
                        help='a .gro file with the final structure (lambda=1)')
    parser.add_argument('--fxtc', type=str, dest='xtc', default=None,
                        help='an xtc file with the trajectory from which initial structures should be drawn (lambda=0)')
    parser.add_argument('--gxtc', type=str, dest='xtc2', default=None,
                        help='an xtc file with the trajectory from which initial structures should be drawn (lambda=1)')
    parser.add_argument('-p', type=str, dest='top', default='topol.top',
                        help='.top file to be passed to grompp, default topol.top; can be a list of space-separated '
                             '.top file names (use quotation marks in this case), e.g. for enhanced sampling in '
                             'intermediate windows')
    parser.add_argument('-n', type=int, dest='njobs', help='number of TI windows to optimize')
    parser.add_argument('-t', type=float, dest='tightness', default=0.25,
                        help='threshold t for convergence, stops when max_prob - min_prob = t*max_prob, default 0.4')
    parser.add_argument('-m', type=str, dest='mpi', default='mpiexec',
                        help='program used to run a parallel job, default mpiexec')
    parser.add_argument('-i', type=str, dest='guess', default=None,
                        help='path to a single-line file containing initial guesses for lambdas, if needed')
    parser.add_argument('-d', dest='prep_only', action='store_true',
                        help='use with -i to only prepare input files based on optimized lambdas')
    parser.add_argument('-o', dest='outfile', action='store_true', default='converged.dat',
                        help='output file for optimized lambdas')
    parser.add_argument('--maxwarn', type=int, dest='maxwarn', default=1,
                        help='max warnings to skip in grompp, default is 1')
    parser.add_argument('--multidir', dest='multidir', action='store_true',
                        help='use in Gromacs versions newer than 2018 where -multi is no longer supported')
    parser.add_argument('--hrex', dest='hrex', action='store_true',
                        help='use Hamiltonian replica exchange as well (needs a Gromacs version patched with Plumed)')
    arguments = parser.parse_args()
    return arguments


def multipliers_from_probs(probs):
    return 2**((probs-0.5)*2)  # 0.5 if prob=0, 2.0 if prob=1, 1.0 if prob=0.5


def update_lambdas(lambdas, probs, learning_rate):
    multipliers = multipliers_from_probs(probs)
    scaled_multipliers = np.mean(multipliers) + learning_rate*(multipliers-np.mean(multipliers))
    deltas = lambdas[1:] - lambdas[:-1]
    updated_deltas = deltas * scaled_multipliers
    normalized_deltas = updated_deltas/sum(updated_deltas)
    new_lambdas = lambdas * 0.0
    new_lambdas[1:] = np.cumsum(normalized_deltas)
    return new_lambdas
   
   
def get_exchange_probs(njobs, logfile='mydyn0.log'):
    """
    log lines starting with 'Repl pr' contain exchange probabilities for 
    alternating sets of windows (e.g. 1-3-5 and 0-2-4-6), with 5 chars per
    window -- need to slice, convert to arrays, sum and divide by half the 
    number of lines; also get ride of the last line if num is odd
    """
    probs = np.zeros(njobs-1)
    with open(logfile) as datafile:
        lines = [line[8:] for line in datafile.readlines() if line.startswith('Repl pr')]
    if len(lines) == 0:
        raise ValueError('no exchange attempts')
    if len(lines) % 2 == 1:
        lines = lines[:-1]
    for line in lines:
        probs += np.array([float(line[5*t:5*(t+1)]+'0') for t in range(njobs-1)])
    if np.isnan(probs).any():
        raise ValueError('no exchange attempts')
    return probs/(len(lines)/2)


def run_grompp_mini(params):
    mini_mdpx, topolx, statex, maxwarn = params
    call('mpiexec -n 1 gmx_mpi grompp -f {state}_{mdp} -p {top} -c mygro{state}.gro -maxwarn {mw} '
         '-o mymini{state}'.format(mdp=mini_mdpx, top=topolx, state=statex, mw=maxwarn), shell=True)


def run_minimization(topol, mini_mdp, njobs, lambdas, mpiexec, serial=False, maxwarn=1, multidir=False):
    if not serial:
        for state in range(njobs):
            set_lambdas(mini_mdp, lambdas, state)
        p = Pool()
        p.map(run_grompp_mini, [(mini_mdp, topol[state], state, maxwarn) for state in range(njobs)])
        if multidir:
            for i in range(njobs):
                os.mkdir(f'mn{i}')
                copy2(f'mymini{i}.tpr', f'mn{i}/mymini.tpr')
            dirlist = ' '.join([f'mn{q}' for q in range(njobs)])
            call('{mpi} gmx_mpi mdrun -deffnm mymini -multidir {dl}'.format(mpi=mpiexec, dl=dirlist), shell=True)
            for i in range(njobs):
                copy2(f'mn{i}/mymini.gro', f'mymini{i}.gro')
                rmtree(f'mn{i}')
        else:
            call('{mpi} gmx_mpi mdrun -deffnm mymini -multi {njobs}'.format(mpi=mpiexec, njobs=njobs), shell=True)
    else:
        for state in range(njobs):
            set_lambdas(mini_mdp, lambdas, state)
            call('gmx_mpi grompp -f {mdp} -p {top} -c mygro{state}.gro -maxwarn {mw} '
                 '-o mymini{state}'.format(mdp=mini_mdp, top=topol, state=state, mw=maxwarn), shell=True)
            call('{mpi} gmx_mpi mdrun -deffnm mymini{state}'.format(mpi=mpiexec, state=state), shell=True)
            copy2('mymini{state}.gro'.format(state=state), 'mygro{state}.gro'.format(state=state+1))


def run_grompp_dyn(params):
    dyn_mdpx, topolx, statex, maxwarn = params
    call('mpiexec -n 1 gmx_mpi grompp -f {state}_{mdp} -p {top} -c mymini{state}.gro -o \
    mydyn{state} -maxwarn {mw}'.format(mdp=dyn_mdpx, top=topolx, state=statex, mw=maxwarn), shell=True)
    

def run_gromacs(topol, dyn_mdp, njobs, lambdas, mpiexec, maxwarn=1, multidir=False, hrex=False):
    for state in range(njobs):
        set_lambdas(dyn_mdp, lambdas, state)
    p = Pool()
    hx = '' if not hrex else '-hrex -plumed plumed.dat '
    p.map(run_grompp_dyn, [(dyn_mdp, topol[state], state, maxwarn) for state in range(njobs)])
    if multidir:
        for i in range(njobs):
            os.mkdir(f'dn{i}')
            copy2(f'mydyn{i}.tpr', f'dn{i}/mydyn.tpr')
            if hrex:
                open(f'dn{i}/plumed.dat', 'a').close()
        dirlist = ' '.join([f'dn{q}' for q in range(njobs)])
        call('{mpi} gmx_mpi mdrun -deffnm mydyn -multidir {dl} -replex 250 {hx}'.format(mpi=mpiexec, dl=dirlist, hx=hx),
             shell=True)
        for i in range(njobs):
            copy2(f'dn{i}/mydyn.gro', f'mydyn{i}.gro')
            copy2(f'dn{i}/mydyn.log', f'mydyn{i}.log')
            rmtree(f'dn{i}')
    else:
        if hrex:
            open('plumed.dat', 'a').close()
        call('{mpi} gmx_mpi mdrun -deffnm mydyn -multi {njobs} -replex 250 {hx}'.format(mpi=mpiexec, njobs=njobs,
                                                                                        hx=hx), shell=True)
    for i in range(njobs):
        copy2('mymini{}.gro'.format(i), 'mygro{}.gro'.format(i))  # as a better guess for subsequent minimization


def set_lambdas(mdp, lambdas, ln):
    lambdas_set = False
    state_set = False
    with open(mdp) as mdpfile:
        text = mdpfile.readlines()
    # need to check if "free-energy = yes" present in mdp
    if not any([x.split()[0] == 'free-energy' and x.split()[2] == 'yes' for x in text if len(x.split()) > 2]):
        raise ValueError('Set \'free-energy = yes\' in {}'.format(mdp))
    for linenumber in range(len(text)):
        line = text[linenumber]
        if line.startswith('init-lambda-state'):
            text[linenumber] = 'init-lambda-state = {}\n'.format(ln)
            state_set = True
        if line.startswith('fep-lambdas'):
            text[linenumber] = 'fep-lambdas = {}\n'.format(' '.join([str(round(a, 5)) for a in lambdas]))
            lambdas_set = True
    if lambdas_set and state_set:  # if all ok, write modified files
        with open("{}_{}".format(ln, mdp), 'w') as mdpfile:
            for line in text:
                mdpfile.write(line)
    else:
        raise ValueError('Couldn\'t find either fep-lambdas or init-lambda-state in file {}, '
                         'cannot proceed with optimization'.format(mdp))


def copy_struct(grofile, target_core_name):
    if grofile.split('.')[-1] == 'gro':
        copy2(grofile, '{}.gro'.format(target_core_name))
    elif grofile.split('.')[-1] == 'pdb':
        call('gmx_mpi editconf -f {pdb} -o {tcn}.gro'.format(pdb=grofile, tcn=target_core_name), shell=True)


def pick_from_xtc(grofile, xtc, initial, final):
    import mdtraj as md
    traj = md.load(xtc, top=grofile)
    nframes = final - initial
    frames = np.linspace(initial, final, nframes+1).astype(int)[1:]
    for num, fr in enumerate(frames, initial):
        curr = traj[fr]
        curr.save_gro('mygro{}.gro'.format(num))
        
    
def prepare_inputs(njobs, dyn_mdp=None, nsteps=None, grofile=None, grofile2=None, xtc=None, xtc2=None):
    """
    Set standard filenames (mygro0.gro, mygro1.gro, ...) 
    and sim length (10000 steps for 50 exchange attemps)
    """
    # first gro files
    if grofile is not None:
        if xtc is None:
            if grofile and grofile2:
                for i in range(njobs//2):
                    copy_struct(grofile, 'mygro{}'.format(i))
                for i in range(njobs//2, njobs):
                    copy_struct(grofile2, 'mygro{}'.format(i))
            else:
                for i in range(njobs):
                    copy_struct(grofile, 'mygro{}'.format(i))
        else:
            if grofile2 is None and xtc:
                pick_from_xtc(grofile, xtc, 0, njobs)
            elif grofile and xtc and xtc2:
                pick_from_xtc(grofile, xtc, 0, njobs // 2)
                pick_from_xtc(grofile, xtc2, njobs // 2, njobs)
    # then mdp file
    if dyn_mdp:
        nsteps_set = False
        with open(dyn_mdp) as mdpfile:
            text = mdpfile.readlines()
        for linenumber in range(len(text)):
            line = text[linenumber]
            if line.startswith('nsteps'):
                text[linenumber] = 'nsteps = {:d}\n'.format(int(nsteps))
                nsteps_set = True
        if nsteps_set:
            with open(dyn_mdp, 'w') as mdpfile:
                for line in text:
                    mdpfile.write(line)
        else:
            raise ValueError('Couldn\'t find nsteps in file {}, cannot proceed with optimization'.format(dyn_mdp))


def clear_files(final=False):
    for i in [x for x in os.listdir('.') if x.endswith('log') or x.endswith('xtc') or x.endswith('#')
              or x.endswith('trr') or x.endswith('xvg') or x.endswith('edr') or x.endswith('tpr')]:
        os.remove(i)
    if final:
        for i in [x for x in os.listdir('.') if x.startswith('mymini') or x.startswith('mydyn')]:
            os.remove(i)
        os.remove('mdout.mdp')
        
        
def log_message(message, mode='a'):
    with open('iterations.dat', mode) as log:
        log.write(message + '\n')


def initialize_lambdas(njobs, initguess):
    if initguess:
        # can start from user-defined lambdas
        lambdas = np.loadtxt(initguess)
        if len(lambdas) != njobs:
            raise ValueError('The number of lambda values specified by user does not match the desired number of jobs')
    else:
        # otherwise initial guess is equally spaced:
        lambdas = np.linspace(0, 1, njobs, endpoint=True)
    return lambdas


if __name__ == '__main__':
    args = parse_args()
    njob = args.njobs
    tightness = args.tightness  # min prob will have to be at least 60% of max prob
    top = args.top
    gro = args.gro  # needs just single .gro file, will get copied
    gro2 = args.gro2 if args.gro2 else None
    converged_outfile = args.outfile
    mpiex = args.mpi
    guess = args.guess
    traj_xtc, traj_xtc2 = args.xtc, args.xtc2
    maxw = args.maxwarn
    mdir = args.multidir
    hrex = args.hrex
    if args.prep_only:
        if args.guess is None:
            raise ValueError('Please provide optimized lambdas with the -i option')
        prepare_minimized(top, gro, gro2, mpiex, njob, guess, traj_xtc, traj_xtc2, maxw, mdir)
        clear_files()
    else:
        lamb, prob = converge_lambdas(tightness, top, gro, gro2, mpiex, njob, guess, traj_xtc, traj_xtc2, maxw, mdir,
                                      hrex)
        with open(converged_outfile, 'w') as out:
            for lb, pr in zip(lamb, prob):
                out.write("{:10.3f}{:10.4f}\n".format(lb, pr))
        clear_files(final=True)
